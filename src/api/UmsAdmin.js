import instance from '@/utils/request'

export function getUmsAdminList () {
  return instance({
    method: 'GET',
    url: '/umsAdmin/'
  })
}

export function getUmsAdminRole (id) {
  return instance({
    method: 'GET',
    url: `/umsAdmin/role/${id}`
  })
}

export function updateUmsAdminRole (id, role) {
  return instance({
    method: 'PUT',
    url: `/umsAdmin/role/${id}`,
    data: role
  })
}

export function pullUserInfo () {
  return instance({
    method: 'GET',
    url: '/umsAdmin/info'
  })
}

export function getUmsAdminOne (id) {
  return instance({
    method: 'GET',
    url: '/umsAdmin/' + id
  })
}

export function saveUmsAdmin (data) {
  return instance({
    method: 'POST',
    url: '/umsAdmin/',
    data: data
  })
}

export function updateUmsAdmin (data, id) {
  return instance({
    method: 'PUT',
    url: '/umsAdmin/' + id,
    data: data
  })
}

export function delUmsAdmin (id) {
  return instance({
    method: 'DELETE',
    url: '/umsAdmin/' + id
  })
}

export function getUmsAdminPage (obj) {
  return instance({
    method: 'POST',
    url: '/umsAdmin/page',
    data: obj
  })
}
