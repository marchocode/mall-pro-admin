import instance from '@/utils/request/'

export function getUmsResourceCategoryList () {
  return instance({
    method: 'GET',
    url: '/umsResourceCategory/'
  })
}

export function getUmsResourceCategoryOne (id) {
  return instance({
    method: 'GET',
    url: '/umsResourceCategory/' + id
  })
}

export function saveUmsResourceCategory (data) {
  return instance({
    method: 'POST',
    url: '/umsResourceCategory/',
    data: data
  })
}

export function updateUmsResourceCategory (data, id) {
  return instance({
    method: 'PUT',
    url: '/umsResourceCategory/' + id,
    data: data
  })
}

export function delUmsResourceCategory (id) {
  return instance({
    method: 'DELETE',
    url: '/umsResourceCategory/' + id
  })
}

export function getUmsResourceCategoryPage (obj) {
  return instance({
    method: 'POST',
    url: '/umsResourceCategory/page',
    data: obj
  })
}
