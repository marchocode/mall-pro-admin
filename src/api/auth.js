import instance from '@/utils/request'

export function login (obj) {
  return instance({
    method: 'POST',
    url: '/auth/login',
    data: obj
  })
}
